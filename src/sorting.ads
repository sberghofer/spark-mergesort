package Sorting
is
   subtype Int_Array_Range is Natural range Natural'First .. Natural'Last - 1;
   type Int_Array is array (Int_Array_Range range <>) of Integer;

   function Sorted
     (A : Int_Array;
      A_First, Length : Natural)
     return Boolean
     with Ghost, Import, Global => Null;

   function Perm
     (A, B : Int_Array;
      A_First, Length : Natural)
     return Boolean
     with Ghost, Import, Global => Null;

  procedure Mergesort
    (A       : in out Int_Array;
     A_First : in     Natural;
     Length  : in     Natural;
     B       : in out Int_Array)
    with
      Depends => ((A, B) => (A, A_First, B, Length)),
      Pre =>
        A'First <= A_First and A_First + Length <= A'Last + 1 and Length <= B'Length and
        B'First <= B'Last,
      Post =>
        Sorted (A, A_First, Length) and Perm (A'Old, A, A_First, Length);

end Sorting;
