OUT_DIR ?= out
SESS_DIR ?= $(OUT_DIR)/proof/sessions

all: prepare_ide
prepare_ide: sparkproof prepare

prepare:
	./prepare_proof

build:
	gprbuild -p -P sorting

sparkproof:
	gnatprove -j4 -P sorting

tests: build
	$(OUT_DIR)/test_sorting

ide: prepare_ide
	cd $(SESS_DIR) && why3 ide --debug fast_wp --extra-expl-prefix "GP_Sloc:" \
	   --extra-expl-prefix "GP_Reason:" sorting

invalidate_proofs:
	cd $(SESS_DIR) && why3 session mod --set-obsolete --debug fast_wp sorting -F

clean:
	rm -rf $(OUT_DIR)
