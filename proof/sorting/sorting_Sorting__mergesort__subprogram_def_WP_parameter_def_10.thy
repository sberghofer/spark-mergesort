theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_10
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_10.xml"

why3_vc WP_parameter_def
proof (cases "length1 = 0")
  case True
  then show ?thesis
    by (simp add: sorted_array_def)
next
  case False
  with
    `mk_map__ref a3 = mk_map__ref a2`
    `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l * 2 < length1 \<longrightarrow>
       sorted_array (Array a2 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + j * l * 2)
         (min (l * 2) (length1 - j * l * 2)) = True`
    `(if length1 - l \<le> l then _ else _) = _`
    `natural_in_range length1`
  show ?thesis
    by (auto simp add: min_absorb2 natural_in_range_def map__content_def)
qed

why3_end

end
