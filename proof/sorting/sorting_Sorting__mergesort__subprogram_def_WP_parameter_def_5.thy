theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_5
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_5.xml"

why3_vc WP_parameter_def
proof (rule eqTrueI)
  let ?array = "\<lambda>a. Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>"

  show "sorted_array (?array a3) (a_first + j * l1 * 2)
    (min (l1 * 2) (length1 - j * l1 * 2))"
  proof (cases "j * l1 * 2 < i2")
    case True
    with `0 \<le> j` `j \<le> 2147483647`
      `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l1 * 2 < i2 \<longrightarrow>
         sorted_array _ _ _ = True`
    have "sorted_array (?array a2) (a_first + j * l1 * 2)
      (min (l1 * 2) (length1 - j * l1 * 2))"
      by simp
    then show ?thesis
    proof (rule sorted_array_ext_same_idx)
      fix i'
      assume H': "a_first + j * l1 * 2 \<le> i'"
        "i' < a_first + j * l1 * 2 + min (l1 * 2) (length1 - j * l1 * 2)"
      then have "i' < a_first + j * l1 * 2 + l1 * 2"
        by auto
      moreover from `1 \<le> l1` `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) \<noteq> _`
        `i2 < length1 \<longrightarrow> WP_parameter_def.mod i2 (l1 * 2) = 0`
        `j * l1 * 2 < i2`
      have "j * (l1 * 2) + l1 * 2 \<le> i2"
        by (simp add: multiple_lt_imp_le mod_def emod_def)
      ultimately have "i' < a_first + i2"
        by simp
      note `\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> a_first`
      also from `0 \<le> j` `1 \<le> l1` have "a_first \<le> a_first + j * l1 * 2"
        by simp
      also from H' have "\<dots> \<le> i'" by simp
      finally have "\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> i'" .
      from H' have "i' < a_first + j * l1 * 2 +
        min (l1 * 2) (length1 - j * l1 * 2)"
        by simp
      also have "min (l1 * 2) (length1 - j * l1 * 2) \<le>
        length1 - j * l1 * 2"
        by simp
      also from `a_first + length1 \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> + 1`
      have "a_first + j * l1 * 2 + (length1 - j * l1 * 2) \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> + 1"
        by simp
      finally have "i' \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int>"
        by simp
      with `i' < a_first + i2` `\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> i'`
        `\<forall>k. \<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> k \<and> k \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> \<longrightarrow> _`
      show "elts (?array a3) i' = elts (?array a2) i'"
        by (simp add: integer_to_int_inject)
    qed
  next
    case False
    note `j * l1 * 2 < i2 + (l1 + min l1 (length1 - l1 - i2))`
    also have "min l1 (length1 - l1 - i2) \<le> l1" by simp
    finally have "j * l1 * 2 < i2 + l1 * 2"
      by (simp add: mult_ac)
    with `1 \<le> l1` `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) \<noteq> _`
      `i2 < length1 \<longrightarrow> WP_parameter_def.mod i2 (l1 * 2) = 0`
    have "j * l1 * 2 \<le> i2"
      by (simp add: multiple_lt_imp_le' mult_ac mod_def emod_def)
    with False
      `sorted_array (?array a3) (a_first + i2) (l1 + min l1 (length1 - l1 - i2)) = True`
    show ?thesis
      by (auto simp add: Orderings.min_def mult_ac)
  qed
qed

why3_end

end
