theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_2
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_2.xml"

why3_vc WP_parameter_def
  by (simp add: perm2_def multiset_of_array_def)

why3_end

end
