theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_8
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_8.xml"

why3_vc WP_parameter_def
proof (rule eqTrueI)
  show "sorted_array (Array a2 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + j * l1 * 2)
    (min (l1 * 2) (length1 - j * l1 * 2))"
  proof (cases "i2 = length1")
    case True
    with `0 \<le> j` `j \<le> 2147483647` `j * l1 * 2 < length1`
      `mk_map__ref a2 = mk_map__ref a1`
      `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l1 * 2 < i2 \<longrightarrow>
         sorted_array (Array a1 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + j * l1 * 2)
           (min (l1 * 2) (length1 - j * l1 * 2)) = True`
    show ?thesis by auto
  next
    case False
    show ?thesis
    proof (cases "j * l1 * 2 < i2")
      case True
      with `0 \<le> j` `j \<le> 2147483647`
        `mk_map__ref a2 = mk_map__ref a1`
        `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l1 * 2 < i2 \<longrightarrow>
           sorted_array (Array a1 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + j * l1 * 2)
             (min (l1 * 2) (length1 - j * l1 * 2)) = True`
      show ?thesis by auto
    next
      case False
      from `j * l1 * 2 < length1` `1 \<le> l1`
        `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) = _`
      have "j * l1 * 2 < i2 + l1 * 2" by auto
      with `i2 < length1 \<longrightarrow> WP_parameter_def.mod i2 (l1 * 2) = 0`
        `i2 \<le> length1` `1 \<le> l1` `i2 \<noteq> length1`
      have "j * l1 * 2 \<le> i2"
        by (auto simp add: multiple_lt_imp_le' mult_ac mod_def emod_def)
      with False `j * l1 * 2 < length1`
        `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) = _`
        `mk_map__ref a2 = mk_map__ref a1`
        `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l1 < length1 - i2 \<longrightarrow>
           sorted_array (Array a1 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + i2 + j * l1)
             (min l1 (length1 - i2 - j * l1)) = True`
      show ?thesis
        by (auto simp add: min_absorb2)
    qed
  qed
qed

why3_end

end
