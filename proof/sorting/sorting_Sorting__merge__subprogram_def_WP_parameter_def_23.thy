theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_23
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_23.xml"

why3_vc WP_parameter_def
  by (simp add: le_array_def)

why3_end

end
