theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_6
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_6.xml"

why3_vc WP_parameter_def
proof (rule eqTrueI)
  let ?array = "\<lambda>a. Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>"

  show "sorted_array (?array a3)
    (a_first + (i2 + (l1 + min l1 (length1 - l1 - i2))) + j * l1)
    (min l1 (length1 - (i2 + (l1 + min l1 (length1 - l1 - i2))) - j * l1))"
  proof (cases "length1 - l1 - i2 < l1")
    case True
    with `0 \<le> j` `j * l1 < length1 - (i2 + (l1 + min l1 (length1 - l1 - i2)))` `1 \<le> l1`
    show ?thesis
      by (simp add: min_absorb2 mult_less_0_iff)
  next
    case False
    from `1 \<le> l1` `0 \<le> j` have "2 + j \<le> (2 + j) * l1" by simp
    also from False `0 \<le> 2147483647 \<longrightarrow> natural_in_range i2` `natural_in_range length1`
      `j * l1 < length1 - (i2 + (l1 + min l1 (length1 - l1 - i2)))`
    have "\<dots> \<le> 2147483647"
      by (simp add: min_absorb1 ring_distribs natural_in_range_def)
    finally have "j \<le> 2147483645" by simp
    moreover from False `j * l1 < length1 - (i2 + (l1 + min l1 (length1 - l1 - i2)))`
    have "(2 + j) * l1 < length1 - i2"
      by (simp add: min_absorb1 ring_distribs)
    ultimately
    have "sorted_array (?array a2)
      (a_first + i2 + (2 + j) * l1) (min l1 (length1 - i2 - (2 + j) * l1))"
      using `0 \<le> j`
        `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow> j * l1 < length1 - i2 \<longrightarrow>
           sorted_array (?array a2) (a_first + i2 + j * l1)
             (min l1 (length1 - i2 - j * l1)) = True`
      by simp
    then have
      "sorted_array (?array a3)
      (a_first + i2 + (2 + j) * l1) (min l1 (length1 - i2 - (2 + j) * l1))"
    proof (rule sorted_array_ext_same_idx)
      fix i'
      assume "a_first + i2 + (2 + j) * l1 \<le> i'"
        "i' < a_first + i2 + (2 + j) * l1 +
           min l1 (length1 - i2 - (2 + j) * l1)"
      note `\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> a_first`
      also from `0 \<le> j`
        `0 \<le> 2147483647 \<longrightarrow> natural_in_range l1`
        `0 \<le> 2147483647 \<longrightarrow> natural_in_range i2`
      have "a_first \<le> a_first + i2 + (2 + j) * l1"
        by (simp add: natural_in_range_def)
      also note `a_first + i2 + (2 + j) * l1 \<le> i'`
      finally have "\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> i'" .
      moreover from
        `i' < a_first + i2 + (2 + j) * l1 +
           min l1 (length1 - i2 - (2 + j) * l1)`
        `a_first + length1 \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> + 1`
      have "i' \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int>"
        by simp
      moreover from `0 \<le> j` `1 \<le> l1`
      have "a_first + i2 + l1 + l1 \<le> a_first + i2 + (2 + j) * l1"
        by simp
      with `a_first + i2 + (2 + j) * l1 \<le> i'`
      have "a_first + i2 + l1 + min l1 (length1 - l1 - i2) \<le> i'"
        by simp
      ultimately show "elts (?array a3) i' = elts (?array a2) i'"
        using `\<forall>k. \<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> k \<and> k \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> \<longrightarrow> _`
        by (simp add: integer_to_int_inject)
    qed
    with False show ?thesis
      by (simp add: min_absorb1 ring_distribs diff_diff_eq [symmetric])
        (simp add: add_ac)
  qed
qed

why3_end

end
