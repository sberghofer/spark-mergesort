theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_28
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_28.xml"

why3_vc WP_parameter_def
  using
    `sorted_array (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + length1) length2 = True`
    `le_array (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) \<lfloor>b__first\<rfloor>\<^sub>\<int>
       (a_first + length1 + j1) (i1 + j1) (length2 - j1) = True`
    `\<lfloor>o1\<rfloor>\<^sub>\<int> = \<lfloor>a (a_first + length1 + j1)\<rfloor>\<^sub>\<int>`
    `_ \<longrightarrow> natural_in_range j1`
  by (auto simp add: sorted_array_def le_array_def integer_to_int_inject natural_in_range_def)

why3_end

end
