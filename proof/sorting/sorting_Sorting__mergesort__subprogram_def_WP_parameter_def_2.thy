theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_2
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_2.xml"

why3_vc WP_parameter_def
  by (simp add: perm_def)

why3_end

end
