theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_22
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_22.xml"

why3_vc WP_parameter_def
  using `i1 \<le> length1` `\<not> i1 < length1`
  by (simp add: le_array_def)

why3_end

end
