theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_20
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_20.xml"

why3_vc WP_parameter_def
  using
    `sorted_array (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) a_first length1 = True`
    `le_array (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>)
       \<lfloor>b__first\<rfloor>\<^sub>\<int> (a_first + i1) (i1 + j1) (length1 - i1) = True`
    `(if \<lfloor>a (a_first + i1)\<rfloor>\<^sub>\<int> \<le> \<lfloor>a (a_first + length1 + j1)\<rfloor>\<^sub>\<int> then _ else _) \<noteq> _`
    `\<lfloor>o1\<rfloor>\<^sub>\<int> = \<lfloor>a (a_first + length1 + j1)\<rfloor>\<^sub>\<int>`
    `_ \<longrightarrow> natural_in_range i1`
  by (auto simp add: sorted_array_def le_array_def integer_to_int_inject natural_in_range_def
    less_eq_integer_def not_le intro: less_le_trans [THEN less_imp_le])

why3_end

end
