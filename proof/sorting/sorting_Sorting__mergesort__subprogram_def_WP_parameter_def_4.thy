theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_4
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_4.xml"

why3_vc WP_parameter_def
  using
    `i2 < length1 \<longrightarrow> WP_parameter_def.mod i2 (l1 * 2) = 0`
    `1 \<le> l1`
    `i2 + (l1 + min l1 (length1 - l1 - i2)) < length1`
  by (auto simp add: mod_def emod_def Orderings.min_def)

why3_end

end
