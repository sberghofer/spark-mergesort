theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_1
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_1.xml"

why3_vc WP_parameter_def
  using `j * 1 < length1`
  by (simp add: min_absorb1 sorted_array_singleton)

why3_end

end
