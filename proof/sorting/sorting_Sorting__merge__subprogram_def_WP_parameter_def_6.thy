theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_6
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_6.xml"

why3_vc WP_parameter_def -- {* Element taken from first array *}
  using
    `sorted_array (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) \<lfloor>b__first\<rfloor>\<^sub>\<int> (i1 + j1) = True`
    `le_array (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>)
       \<lfloor>b__first\<rfloor>\<^sub>\<int> (a_first + i1) (i1 + j1) (length1 - i1) = True`
    `\<lfloor>o1\<rfloor>\<^sub>\<int> = \<lfloor>a (a_first + i1)\<rfloor>\<^sub>\<int>`
    `i1 < length1`
  by (auto simp add: sorted_array_def le_array_def integer_to_int_inject)

why3_end

end
