theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_14
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_14.xml"

why3_vc WP_parameter_def
  using
    `perm2 (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>)
       a_first (a_first + length1) \<lfloor>b__first\<rfloor>\<^sub>\<int> i1 j1 = True`
    `\<lfloor>o1\<rfloor>\<^sub>\<int> = \<lfloor>a (a_first + length1 + j1)\<rfloor>\<^sub>\<int>`
    `_ \<longrightarrow> natural_in_range i1` `_ \<longrightarrow> natural_in_range j1`
  by (simp add: perm2_def natural_in_range_def
    add_ac multiset_of_array_sum' multiset_of_array_singleton
    integer_to_int_inject)
    (simp add: add_ac multiset_of_array_def)

why3_end

end
