theory sorting_Sorting__merge__subprogram_def_WP_parameter_def_30
imports Sorting
begin

why3_open "sorting_Sorting__merge__subprogram_def_WP_parameter_def_30.xml"

why3_vc WP_parameter_def
proof -
  have H: "eq_array (Array a1 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) a_first (length1 + length2)
    (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) \<lfloor>b__first\<rfloor>\<^sub>\<int>"
  proof (rule eq_arrayI)
    fix j
    assume "a_first \<le> j" "j < a_first + (length1 + length2)"
    with
      `\<forall>j. \<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> j \<and> j \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> \<longrightarrow> _`
      `\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> a_first`
      `a_first + length1 + length2 \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> + 1`
      `mk_map__ref b1 = mk_map__ref b`
    show "elts (Array a1 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) j =
      elts (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>) (\<lfloor>b__first\<rfloor>\<^sub>\<int> + j - a_first)"
      by (simp add: integer_to_int_inject)
  qed

  from
    `perm2 (Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (Array b \<langle>\<lfloor>b__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>b__last\<rfloor>\<^sub>\<int>\<rangle>)
       a_first (a_first + length1) \<lfloor>b__first\<rfloor>\<^sub>\<int> i j = True`
    `\<not> (i < length1 \<or> j < length2)` `i \<le> length1` `j \<le> length2`
    `natural_in_range length1` `natural_in_range length2`
  show ?thesis
    by (simp add: perm_def perm2_def multiset_of_array_ext [OF H] multiset_of_array_sum
      natural_in_range_def)
qed

why3_end

end
