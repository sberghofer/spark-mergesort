theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_3
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_3.xml"

why3_vc WP_parameter_def
  using
    `\<forall>j. 0 \<le> j \<and> j \<le> 2147483647 \<longrightarrow>
       j * l1 < length1 - i2 \<longrightarrow>
       sorted_array (Array a2 \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>) (a_first + i2 + j * l1)
         (min l1 (length1 - i2 - j * l1)) = True`
    `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) \<noteq> _`
  by (auto elim: allE [of _ 1] simp add: min_absorb1 sign_simps)

why3_end

end
