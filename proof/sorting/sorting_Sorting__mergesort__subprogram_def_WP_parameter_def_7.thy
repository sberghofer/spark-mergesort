theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_7
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_7.xml"

why3_vc WP_parameter_def
proof -
  let ?array = "\<lambda>a. Array a \<langle>\<lfloor>a__first\<rfloor>\<^sub>\<int>\<dots>\<lfloor>a__last\<rfloor>\<^sub>\<int>\<rangle>"
thm assms
  from
    `\<forall>k. \<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> k \<and> k \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> \<longrightarrow> _`
    `\<lfloor>a__first\<rfloor>\<^sub>\<int> \<le> a_first`
    `a_first + length1 \<le> \<lfloor>a__last\<rfloor>\<^sub>\<int> + 1`
    `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) \<noteq> _` `1 \<le> l1`
    `_ \<longrightarrow> natural_in_range i2`
  have "multiset_of_array (?array a2) a_first i2 =
       multiset_of_array (?array a3) a_first i2"
    "multiset_of_array (?array a2)
       (a_first + i2 + (l1 + min l1 (length1 - l1 - i2)))
       (length1 - i2 - (l1 + min l1 (length1 - l1 - i2))) =
     multiset_of_array (?array a3)
       (a_first + i2 + (l1 + min l1 (length1 - l1 - i2)))
       (length1 - i2 - (l1 + min l1 (length1 - l1 - i2)))"
    by (auto intro: multiset_of_array_ext'
      simp add: mk_bounds_fst mk_bounds_snd integer_to_int_inject natural_in_range_def)
  with
    `perm (?array a2) (?array a3)
       (a_first + i2) (l1 + min l1 (length1 - l1 - i2)) = True`
  have
    "multiset_of_array (?array a2) a_first i2 +
     multiset_of_array (?array a2) (a_first + i2)
       (l1 + min l1 (length1 - l1 - i2)) +
     multiset_of_array (?array a2) (a_first + i2 +
       (l1 + min l1 (length1 - l1 - i2)))
       (length1 - i2 - (l1 + min l1 (length1 - l1 - i2))) =
     multiset_of_array (?array a3) a_first i2 +
     multiset_of_array (?array a3) (a_first + i2)
       (l1 + min l1 (length1 - l1 - i2)) +
     multiset_of_array (?array a3) (a_first + i2 +
       (l1 + min l1 (length1 - l1 - i2)))
       (length1 - i2 - (l1 + min l1 (length1 - l1 - i2)))"
    by (simp add: perm_def)
  with `(if (if i2 < length1 - l1 then _ else _) \<noteq> _ then _ else _) \<noteq> _`
    `perm (?array a1) (?array a2) a_first length1 = True`
    `_ \<longrightarrow> natural_in_range i2`
    `_ \<longrightarrow> natural_in_range l1`
  show ?thesis
    by (simp add: multiset_of_array_sum [symmetric] add.assoc perm_def natural_in_range_def)
qed

why3_end

end
