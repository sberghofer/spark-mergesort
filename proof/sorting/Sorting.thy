theory Sorting
imports SPARK2014 "~~/src/HOL/Library/Multiset"
begin

hide_type (open) Code_Numeral.integer

(**** Sorting__int_array ****)

type_synonym bounds = "integer \<times> integer"

definition mk_bounds :: "int \<Rightarrow> int \<Rightarrow> bounds" ("\<langle>_\<dots>_\<rangle>") where
  "\<langle>f\<dots>l\<rangle> = (integer_of_int f, integer_of_int l)"

lemma mk_bounds_eqs:
  assumes "integer_in_range f" "integer_in_range l"
  shows "integer_to_int (fst \<langle>f\<dots>l\<rangle>) = f"
  and "integer_to_int (snd \<langle>f\<dots>l\<rangle>) = l"
  using assms
  by (simp_all add: mk_bounds_def integer_of_int_inverse)

lemma mk_bounds_fst: "fst \<langle>\<lfloor>f\<rfloor>\<^sub>\<int>\<dots>\<lfloor>l\<rfloor>\<^sub>\<int>\<rangle> = f"
  by (simp add: mk_bounds_def integer_to_int_inverse)

lemma mk_bounds_snd: "snd \<langle>\<lfloor>f\<rfloor>\<^sub>\<int>\<dots>\<lfloor>l\<rfloor>\<^sub>\<int>\<rangle> = l"
  by (simp add: mk_bounds_def integer_to_int_inverse)

lemma mk_bounds_expand:
  "\<lfloor>fst b\<rfloor>\<^sub>\<int> = \<lfloor>fst \<langle>\<lfloor>f\<rfloor>\<^sub>\<int>\<dots>\<lfloor>l\<rfloor>\<^sub>\<int>\<rangle>\<rfloor>\<^sub>\<int> \<Longrightarrow> \<lfloor>snd b\<rfloor>\<^sub>\<int> = \<lfloor>snd \<langle>\<lfloor>f\<rfloor>\<^sub>\<int>\<dots>\<lfloor>l\<rfloor>\<^sub>\<int>\<rangle>\<rfloor>\<^sub>\<int> \<Longrightarrow> b = \<langle>\<lfloor>f\<rfloor>\<^sub>\<int>\<dots>\<lfloor>l\<rfloor>\<^sub>\<int>\<rangle>"
  by (rule prod_eqI)
    (simp_all add: mk_bounds_fst mk_bounds_snd integer_to_int_inject)

datatype array = Array "int \<Rightarrow> integer" bounds

definition rt :: "array \<Rightarrow> bounds" where
  "rt v = (case v of Array a r \<Rightarrow> r)"

definition elts :: "array \<Rightarrow> int \<Rightarrow> integer" where
  "elts v = (case v of Array a r \<Rightarrow> a)"

lemma elts_Array [simp]: "elts (Array a r) = a"
  by (simp add: elts_def)

why3_types
  Sorting__int_array.t = bounds
  "Sorting__int_array.__t" = array

why3_consts
  Sorting__int_array.mk = mk_bounds
  Sorting__int_array.first = fst
  Sorting__int_array.last = snd

why3_defs
  Sorting__int_array.elts = elts_def

why3_thms
  Sorting__int_array.mk_def = mk_bounds_eqs

(**** Proof Functions ****)

definition
  multiset_of_array :: "array \<Rightarrow> int \<Rightarrow> int \<Rightarrow> integer multiset"
where
  "multiset_of_array a i l = (\<Sum>j=i..<i+l. {# elts a j #})"

definition
  le_array :: "array \<Rightarrow> array \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool"
where
  "le_array a\<^sub>1 a\<^sub>2 i\<^sub>1 i\<^sub>2 l\<^sub>1 l\<^sub>2 = (\<forall>j\<^sub>1\<in>{i\<^sub>1..<i\<^sub>1+l\<^sub>1}. \<forall>j\<^sub>2\<in>{i\<^sub>2..<i\<^sub>2+l\<^sub>2}. elts a\<^sub>1 j\<^sub>1 \<le> elts a\<^sub>2 j\<^sub>2)"

definition
  sorted_array :: "array \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool"
where
  "sorted_array a i l = (\<forall>k\<in>{i..<i+l}. \<forall>j\<in>{i..k}. elts a j \<le> elts a k)"

lemma multiset_of_array_sum:
  "0 \<le> l \<Longrightarrow> 0 \<le> l' \<Longrightarrow>
   multiset_of_array a i (l + l') =
   multiset_of_array a i l + multiset_of_array a (i + l) l'"
  using comm_monoid_add_class.setsum.union_disjoint [of "{i..<i+l}" "{i+l..<i+l+l'}" "\<lambda>j. {# elts a j #}"]
  by (simp add: multiset_of_array_def ivl_disj_un add_ac)

lemma multiset_of_array_sum':
  "0 \<le> l \<Longrightarrow> 0 \<le> l' \<Longrightarrow>
   multiset_of_array a i (l' + l) =
   multiset_of_array a i l + multiset_of_array a (i + l) l'"
  by (simp add: multiset_of_array_sum add.commute [of l' l])

lemma multiset_of_array_singleton:
  "multiset_of_array a i 1 = {# elts a i #}"
  by (simp add: multiset_of_array_def atLeastLessThanPlusOne_atLeastAtMost_int)

lemma sorted_array_singleton: "sorted_array a i 1"
  apply (auto simp add: sorted_array_def)
  apply (drule order_trans)
  apply assumption
  apply simp
  done

definition
  eq_array :: "array \<Rightarrow> int \<Rightarrow> int \<Rightarrow> array \<Rightarrow> int \<Rightarrow> bool"
where
  "eq_array a a_fst l b b_fst =
     (\<forall>i. a_fst \<le> i \<and> i < a_fst + l \<longrightarrow> elts a i = elts b (b_fst + i - a_fst))"

lemma eq_arrayI:
  "(\<And>i. a_fst \<le> i \<Longrightarrow> i < a_fst + l \<Longrightarrow> elts a i = elts b (b_fst + i - a_fst)) \<Longrightarrow>
   eq_array a a_fst l b b_fst"
  by (simp add: eq_array_def)

lemma sorted_array_ext:
  assumes H: "sorted_array b b_fst l"
  and H': "eq_array a a_fst l b b_fst"
  shows "sorted_array a a_fst l"
  using H
  apply (simp add: sorted_array_def)
  apply (rule ballI)+
  apply (drule_tac x="b_fst + k - a_fst" in bspec)
  apply simp
  apply (drule_tac x="b_fst + j - a_fst" in bspec)
  apply simp
  apply (simp add: H' [unfolded eq_array_def])
  done

lemmas sorted_array_ext' = sorted_array_ext [OF _ eq_arrayI]

lemma sorted_array_ext_same_idx:
  assumes "sorted_array b a_fst l"
  and "\<And>i. a_fst \<le> i \<Longrightarrow> i < a_fst + l \<Longrightarrow> elts a i = elts b i"
  shows "sorted_array a a_fst l"
  using assms
  by (simp add: sorted_array_ext')

lemma multiset_of_array_ext:
  "eq_array a a_fst l b b_fst \<Longrightarrow>
   multiset_of_array a a_fst l = multiset_of_array b b_fst l"
  using setsum.reindex [of "\<lambda>j. b_fst + j - a_fst" "{b_fst..<b_fst}"]
  apply (simp add: multiset_of_array_def eq_array_def)
  apply (rule setsum.reindex_cong [of "\<lambda>j. j + (a_fst - b_fst)"])
  apply (rule inj_onI)
  apply simp
  apply (simp add:
    image_add_int_atLeastLessThan [symmetric, of b_fst "b_fst + l"]
    image_add_int_atLeastLessThan [symmetric, of a_fst "a_fst + l"]
    image_image)
  apply simp
  done

lemmas multiset_of_array_ext' = multiset_of_array_ext [OF eq_arrayI]

lemma le_array_ext:
  "eq_array a a_fst l b b_fst \<Longrightarrow> eq_array c c_fst l' d d_fst \<Longrightarrow>
   le_array b d b_fst d_fst l l' \<Longrightarrow> le_array a c a_fst c_fst l l'"
  by (auto simp add: eq_array_def le_array_def)

lemma multiple_lt_imp_le:
  assumes "0 < (m::int)" "i mod m = 0" "j * m < i"
  shows "j * m + m \<le> i"
proof -
  from `i mod m = 0` obtain k where "i = m * k"
    by (auto simp add: zmod_eq_0_iff)
  with `0 < m` `j * m < i`
  have "m * (j + 1) \<le> m * k" by simp
  then show ?thesis using `i = m * k`
    by (simp add: ring_distribs mult_ac)
qed

lemma multiple_lt_imp_le':
  assumes "0 < (m::int)" "i mod m = 0" "j * m < i + m"
  shows "j * m \<le> i"
proof -
  from `i mod m = 0` obtain k where "i = m * k"
    by (auto simp add: zmod_eq_0_iff)
  with `j * m < i + m`
  have "j * m < (k + 1) * m"
    by (simp add: ring_distribs mult_ac)
  with `0 < m` `i = m * k` show ?thesis
    by simp
qed

definition perm where
  "perm a b i l = (multiset_of_array a i l = multiset_of_array b i l)"

definition perm2 where
  "perm2 a b i\<^sub>1 i\<^sub>2 j l\<^sub>1 l\<^sub>2 =
     (multiset_of_array a i\<^sub>1 l\<^sub>1 + multiset_of_array a i\<^sub>2 l\<^sub>2 =
      multiset_of_array b j (l\<^sub>1 + l\<^sub>2))"

why3_consts
  Sorting__sorted.sorted = sorted_array
  Sorting__perm.perm = perm
  Sorting__perm2.perm2 = perm2
  Sorting__le_array.le_array = le_array

end
