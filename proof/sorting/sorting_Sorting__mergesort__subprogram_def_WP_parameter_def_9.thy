theory sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_9
imports Sorting
begin

why3_open "sorting_Sorting__mergesort__subprogram_def_WP_parameter_def_9.xml"

why3_vc WP_parameter_def
  using `j * (2 * l1) < length1` `0 \<le> j` `j \<le> 2147483647`
  `\<forall>j. _ \<longrightarrow> _ \<longrightarrow> sorted_array (Array a2 _) _ _ = True`
  by (simp add: mult_ac)

why3_end

end
